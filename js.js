// 1. navigation - done
// 2. add event listener - done
// 3. create data structure with quotes - done
// 4. get random quote
// 5. display random quote

// Navigation
var p = document.getElementById('quote');
var button = document.querySelector('button');

var quotes = [
    [
      ' Будем работать на ошибками, чтобы ошибаться правильно.'


    ],

    [
       'Каждый живет, как хочет, и расплачивается за это сам.ть с первого шага.'

    ],

    [
        'Чудо не может быть неизменным.'


    ],

    [
        "Чем хуже игра политиков, тем охотнее они хватаются за хорошую мину."
    ]
];

function getRandomQuote() {

    var quote = quotes[Math.floor(Math.random() * quotes.length)];
    p.innerHTML = quote[0];
}

function showQuotes() {
    if (p.style.display == 'block') {
        p.style.display = "none";
    } else {
        p.style.display = "block";
    }
}


// Get random quote on init
getRandomQuote();

// Add event listener for random quote button
button.addEventListener('click', getRandomQuote, );
button.addEventListener('click', showQuotes, );
